//
//  InviteViewController.swift
//  DatingApp
//
//  Created by Adnan on 20/02/21.
//

import UIKit

class InviteViewController: UIViewController {

    @IBOutlet weak var btn_RefInvite: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btn_RefInvite.layer.masksToBounds = true
        self.btn_RefInvite.layer.cornerRadius = 5
    }
    

    @IBAction func btn_Invite(_ sender: UIButton) {
        let text = ""
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }

}
